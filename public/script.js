angular.module('BlankApp', ['ngMaterial'])
.controller('AppCtrl', function($scope) {
  $scope.templateUrl = null;
  $scope.lastActive = 'navhome';
  $scope.changePage = function(url, activeid) {
      this.templateUrl = url;
      console.log(url);
      
      document.getElementById(this.lastActive).classList.remove('active');
      document.getElementById(activeid).classList.add('active');
      this.lastActive = activeid;
  }
});
